﻿#pragma once
#include <windows.h>;

template <class T>
class IterableCollection
{
public:
	T* Iterator;
	virtual T* CreateIterator(const HANDLE handle, const long collectionSize, const long clusterSize) = 0;

	virtual ~IterableCollection()
	{
		delete Iterator;
	}

protected:
	IterableCollection()
	{
		Iterator = nullptr;
	}
};
