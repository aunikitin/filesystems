#include "pch.h"
#include <stdexcept>
#include "BaseFileSystemReader.h"
#include "NTFSFileSystemReader.h"
#include "FileSystemReaderFactory.h"
#include "ExFATFileSystemReader.h"
#include "Fat32FileSystemReader.h"

BaseFileSystemReader* FileSystemReaderFactory::CreateFileSystemReader(const HANDLE handle, BYTE *buffer)
{
	const NTFSBaseSystemInfo* ntfsBaseSystemInfo = reinterpret_cast<NTFSBaseSystemInfo*>(buffer);
	std::string oemName(reinterpret_cast<char const*>(ntfsBaseSystemInfo -> OemName));
	if(oemName.find("NTFS") != std::string::npos)
	{
		return new NTFSFileSystemReader(handle, buffer);
	}

	const ExFATBaseFileSystemInfo* exfatBaseSystemInfo = reinterpret_cast<ExFATBaseFileSystemInfo*>(buffer);
	oemName = reinterpret_cast<char const*>(exfatBaseSystemInfo->OemName);
	if (oemName.find("EXFAT") != std::string::npos)
	{
		return new ExFATFileSystemReader(handle, buffer);
	}

	const Fat32BaseSystemInfo* fat32BaseSystemInfo = reinterpret_cast<Fat32BaseSystemInfo*>(buffer);
	oemName = reinterpret_cast<char const*>(fat32BaseSystemInfo->OemName);
	if (oemName.find("MSDOS5.0") != std::string::npos)
	{
		return new Fat32FileSystemReader(handle, buffer);
	}

	throw std::logic_error("FileSystem " + oemName + " not implemented yet.");
}

