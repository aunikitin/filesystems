#include "pch.h"
#include <iostream>
#include <string>
#include <windows.h>
#include "BaseFileSystemReader.h"
#include "FileSystemReaderFactory.h"
#include "Commands.h"
#include <algorithm>

HANDLE OpenFile(const std::wstring& wFileName, const std::string& fileName);
void ReadFile(HANDLE handle, BYTE *buffer, const std::string& fileName);
void ShowFileSystemInfo(BYTE *buffer, HANDLE handle);
void ShowSpecificCluster(BaseFileSystemReader* reader);
void ShowAllClusters(BaseFileSystemReader* reader);

std::string ShowClusterDataDialog();
std::string ShowChooseDiskDialog();
std::string ShowCommandMenu();

Commands TranslateCommand(const std::string& command);

int main()
{
	std::string diskIdentificator = ShowChooseDiskDialog();
	while(diskIdentificator.compare("exit") == -1)
	{
		auto fileName = R"(\\.\)" + diskIdentificator + ":";
		const std::wstring wFileName(fileName.begin(), fileName.end());
		HANDLE handle;
		try
		{
			handle = OpenFile(wFileName, fileName);
		}
		catch(std::runtime_error error)
		{
			std::cout << error.what() << std::endl;
			diskIdentificator = ShowChooseDiskDialog();
			continue;
		}

		BYTE buffer[1024];
		try
		{
			ReadFile(handle, buffer, fileName);
		}
		catch (std::runtime_error error)
		{
			std::cout << error.what() << std::endl;
			diskIdentificator = ShowChooseDiskDialog();
			continue;
		}

		try
		{
			ShowFileSystemInfo(buffer, handle);
		}
		catch(std::runtime_error error)
		{
			std::cout << error.what() << std::endl;
			diskIdentificator = ShowChooseDiskDialog();
			continue;
		}
		

		CloseHandle(handle);
		diskIdentificator = ShowChooseDiskDialog();
	}
	return 0;
}

std::string ShowChooseDiskDialog()
{
	std::cout << "Enter disk identificator to open(Type 'exit' to exit):" << std::endl;
	std::string command;
	std::getline(std::cin, command);

	return command;
}

HANDLE OpenFile(const std::wstring& wFileName, const std::string& fileName)
{
	const auto handle = CreateFileW(wFileName.c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
		nullptr, OPEN_EXISTING, 0, nullptr);

	if (handle == INVALID_HANDLE_VALUE)
	{
		throw std::runtime_error("There're some problems with " + fileName + " opening.");
	}

	return handle;
}

void ReadFile(const HANDLE handle, BYTE *buffer, const std::string& fileName)
{
	const DWORD bytesToRead = 1024;
	DWORD bytesRead;

	// ������ ������
	const bool readResult = ReadFile(
		handle,
		buffer,
		bytesToRead,
		&bytesRead,
		nullptr
	);

	if (!readResult || bytesRead != bytesToRead)
	{
		throw std::runtime_error("There're some problems with " + fileName + " reading.");
	}
}

std::string ShowCommandMenu()
{
	const std::string enterClusterNumberCommand = "A. Enter cluster number to read,";
	const std::string showAllClustersCommand = "B. Show all clusters.";
	const std::string showEvenClustersCommand = "C. Show even clusters(show cluster number for example).";

	std::cout << "Choose command(Type 'exit' to change disk):" << std::endl;
	std::cout << enterClusterNumberCommand << std::endl;
	std::cout << showAllClustersCommand << std::endl;
	std::cout << showEvenClustersCommand << std::endl;

	std::string command;
	std::getline(std::cin, command);
	std::transform(command.begin(), command.end(), command.begin(), ::tolower);

	return command;
}


std::string ShowClusterDataDialog()
{
	std::cout << "Enter cluster number to read(Type 'exit' to change disk):" << std::endl;
	std::string command;
	std::getline(std::cin, command);

	return command;
}

void ShowSpecificCluster(BaseFileSystemReader* reader)
{
	std::string command = ShowClusterDataDialog();
	while (command.compare("exit") == -1)
	{
		if(command != "")
		{
			reader->ShowData(std::stoi(command));
		}
		command = ShowClusterDataDialog();
	}
}

void ShowAllClusters(BaseFileSystemReader* reader)
{
	reader->ShowData();
}

void ShowEvenClusters(BaseFileSystemReader* reader)
{
	EvenClusterIteratorDecorator* iterator = reader->Clusters->GetEvenIterator();
	while(iterator->HasMore())
	{
		Cluster* cluster = iterator->GetNext();
		std::cout << iterator->GetCurrentPosition() << std::endl;
	}
	reader->ShowData();
}

void ShowFileSystemInfo(BYTE *buffer, HANDLE handle)
{
	BaseFileSystemReader *fileSystemReader;
	try
	{
		fileSystemReader = FileSystemReaderFactory::CreateFileSystemReader(handle, buffer);
	}
	catch (std::runtime_error error)
	{
		std::cout << error.what() << std::endl;
		throw;
	}

	const std::string fileSystemName = fileSystemReader->GetFileSystemName();
	std::cout << fileSystemName << " was opened." << std::endl;
	fileSystemReader->ShowFileSystemInfo();
	std::cout << std::endl;

	std::string command = ShowCommandMenu();
	Commands castedCommand = TranslateCommand(command);
	while(castedCommand != Exit)
	{
		switch(castedCommand)
		{
			case EnterClusterNumber:
				ShowSpecificCluster(fileSystemReader);
				break;
			case ShowAll:
				ShowAllClusters(fileSystemReader);
				break;
			case TryDecorator:
				ShowEvenClusters(fileSystemReader);
				break;
			default:
				std::cout << "Wrong input. Try again:" << std::endl;
		}

		command = ShowCommandMenu();
		castedCommand = TranslateCommand(command);
	}

	delete fileSystemReader;
	std::cout << fileSystemName << " was closed." << std::endl;
}

Commands TranslateCommand(const std::string& command)
{
	if(command.compare("a") == 0)
	{
		return EnterClusterNumber;
	}

	if(command.compare("exit") == 0)
	{
		return Exit;
	}

	if(command.compare("b") == 0)
	{
		return ShowAll;
	}

	if (command.compare("c") == 0)
	{
		return TryDecorator;
	}

	return WrongCommand;
}
