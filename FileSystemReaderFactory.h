#pragma once
#include "BaseFileSystemReader.h"
#include <windows.h>

class FileSystemReaderFactory final
{
public:
	~FileSystemReaderFactory() = default;
	static BaseFileSystemReader* CreateFileSystemReader(HANDLE handle, BYTE *buffer);
private:
	FileSystemReaderFactory() = default;
};
