﻿#pragma once
#include <string>
#include <windows.h>;
#include "ClusterCollection.h"

class BaseFileSystemReader
{
public:
	explicit BaseFileSystemReader(const HANDLE handle)
	{
		TotalClusters = 0;
		this->Handle = handle;
		SetFilePointerEx(this->Handle, { 0 }, nullptr, FILE_BEGIN);
		Clusters = nullptr;
	}
	virtual ~BaseFileSystemReader()
	{
		delete Clusters;
	};
	virtual std::string GetFileSystemName() = 0;
	virtual void ShowFileSystemInfo() = 0;
	void ShowData(int clusterNumber) const;
	void ShowData() const;
	ClusterCollection* Clusters;
protected:
	HANDLE Handle = nullptr;
	DWORD TotalClusters;
	BYTE* ReadData(const DWORD startPosition, const DWORD bytesToRead) const;
};
