#pragma once
#pragma pack(push, 1)
#include "BaseFileSystemReader.h"

struct NTFSBaseSystemInfo
{
	BYTE Padding[3];
	BYTE OemName[8];
};

struct NTFSFileSystemInfo final : NTFSBaseSystemInfo
{
	WORD BytesPerSector;
	BYTE SectorsPerCluster;
	WORD ReservedSectors;
	BYTE UnusedData[24];
	LONGLONG TotalSectors;
	WORD BytesPerCluster;
	LONGLONG TotalClusters;
};

#pragma pack(pop)

class NTFSFileSystemReader final :
	public BaseFileSystemReader
{
public:
	explicit NTFSFileSystemReader(const HANDLE handle, BYTE *buffer) : BaseFileSystemReader(handle)
	{
		this->_fileSystemInfo = reinterpret_cast<NTFSFileSystemInfo*>(buffer);
		this->_fileSystemInfo->BytesPerCluster = this->_fileSystemInfo->BytesPerSector * this->_fileSystemInfo->SectorsPerCluster;
		this->_fileSystemInfo->TotalClusters = this->_fileSystemInfo->TotalSectors / this->_fileSystemInfo->SectorsPerCluster;
		TotalClusters = this->_fileSystemInfo->TotalClusters;
		Clusters = new ClusterCollection(handle, this->_fileSystemInfo->TotalClusters, this->_fileSystemInfo->BytesPerCluster);
	}
	~NTFSFileSystemReader() = default;
	void ShowFileSystemInfo() override;

	std::string GetFileSystemName() override
	{
		return "NTFS";
	}
private:
	NTFSFileSystemInfo* _fileSystemInfo;
};

