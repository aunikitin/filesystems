#pragma once
#include "IterableCollection.h"
#include "ClusterIterator.h"
#include "EvenClusterIteratorDecorator.h"

class ClusterCollection: IterableCollection<ClusterIterator>
{
public:
	ClusterCollection(const HANDLE handle, const long collectionSize, const long clusterSize)
	{
		Iterator = ClusterCollection::CreateIterator(handle, collectionSize, clusterSize);
		_evenIterator = new EvenClusterIteratorDecorator(collectionSize, Iterator);
	}

	~ClusterCollection()
	{
		delete _evenIterator;
	}

	ClusterIterator* GetIterator() const
	{
		return Iterator;
	}

	EvenClusterIteratorDecorator* GetEvenIterator() const
	{
		return _evenIterator;
	}
private:
	EvenClusterIteratorDecorator* _evenIterator;
	ClusterIterator* CreateIterator(const HANDLE handle, const long collectionSize, const long clusterSize) override
	{
		if (Iterator == nullptr)
		{
			Iterator = new ClusterIterator(handle, collectionSize, clusterSize);
		}
		return Iterator;
	}
};

