#include "pch.h"
#include "EvenClusterIteratorDecorator.h"


Cluster* EvenClusterIteratorDecorator::GetNext()
{
	const long currentClusterNumber = _iterator->GetCurrentPosition();
	if (currentClusterNumber % 2 == 0)
	{
		return _iterator->GetNext();
	}
	_iterator->SetClusterNumber(currentClusterNumber + 1);
	return GetNext();
}

bool EvenClusterIteratorDecorator::HasMore()
{
	return _iterator->HasMore();
}
