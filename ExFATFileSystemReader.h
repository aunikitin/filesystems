#pragma once
#pragma pack(push, 1)
#include "BaseFileSystemReader.h"

struct ExFATBaseFileSystemInfo
{
	BYTE Padding[3];
	BYTE OemName[8];
};

struct ExFATFileSystemInfo: ExFATBaseFileSystemInfo
{
	BYTE UnusedData[61];
	LONGLONG TotalSectors; // VolumeLength
	BYTE UnusedData2[12];
	DWORD TotalClusters;
	BYTE UnusedData3[12];
	BYTE BytesPerSectorShift;
	BYTE SectorsPerClusterShift;
	LONGLONG BytesPerCluster;
	LONGLONG BytesPerSector;
	LONGLONG SectorsPerCluster;
};

#pragma pack(pop)


class ExFATFileSystemReader final : public BaseFileSystemReader
{
public:
	explicit ExFATFileSystemReader(const HANDLE handle, BYTE *buffer) : BaseFileSystemReader(handle)
	{
		this->_fileSystemInfo = reinterpret_cast<ExFATFileSystemInfo*>(buffer);
		this->_fileSystemInfo->BytesPerSector = pow(2, static_cast<int>(this->_fileSystemInfo->BytesPerSectorShift));
		this->_fileSystemInfo->SectorsPerCluster = pow(2, static_cast<int>(this->_fileSystemInfo->SectorsPerClusterShift));
		this->_fileSystemInfo->BytesPerCluster = this->_fileSystemInfo->BytesPerSector * this->_fileSystemInfo->SectorsPerCluster;
		TotalClusters = this->_fileSystemInfo->TotalClusters;
		Clusters = new ClusterCollection(handle, this->_fileSystemInfo->TotalClusters, this->_fileSystemInfo->BytesPerCluster);
	}
	~ExFATFileSystemReader() = default;
	void ShowFileSystemInfo() override;

	std::string GetFileSystemName() override
	{
		return "ExFAT";
	}
private:
	ExFATFileSystemInfo* _fileSystemInfo;
};

