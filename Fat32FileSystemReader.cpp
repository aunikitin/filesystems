#include "pch.h"
#include "Fat32FileSystemReader.h"

using namespace std;
void Fat32FileSystemReader::ShowFileSystemInfo()
{
	cout << "Bytes per sector: " << _fileSystemInfo->BytesPerSector << endl;
	cout << "Sectors per cluster: " << static_cast<int>(_fileSystemInfo->SectorsPerCluster) << endl;
	cout << "Reserved sectors: " << _fileSystemInfo->ReservedSectors << endl;
	cout << "Bytes per cluster: " << _fileSystemInfo->BytesPerCluster << endl;
	cout << "Total sectors: " << _fileSystemInfo->TotalSectors << endl;
	cout << "Total clusters: " << _fileSystemInfo->TotalClusters << endl;
}

void Fat32FileSystemReader::FillMainFileSystemInfo() const
{
	const int totalSectors = _fileSystemInfo->FatSize16 != 0 ? _fileSystemInfo->FatSize16 : _fileSystemInfo->FatSize32;

	const int fatStartSector = _fileSystemInfo->ReservedSectors;
	const long fatSectors = static_cast<int>(_fileSystemInfo->NumbersOfFat) * totalSectors;
	const long rootDirectoryStartSector = fatStartSector + fatSectors;
	const long rootDirectorySectors = (32 * _fileSystemInfo->RootEntityCount + _fileSystemInfo->BytesPerSector - 1) / _fileSystemInfo->BytesPerSector;
	const long dataStartSector = rootDirectorySectors + rootDirectoryStartSector;
	const long dataSectorsCount = totalSectors - dataStartSector;

	const int sectorsPerCluster = static_cast<unsigned short>(_fileSystemInfo->SectorsPerCluster);
	_fileSystemInfo->TotalClusters = dataSectorsCount / sectorsPerCluster;
	_fileSystemInfo->BytesPerCluster = _fileSystemInfo->BytesPerSector*sectorsPerCluster;
	_fileSystemInfo->TotalSectors = totalSectors;
}

