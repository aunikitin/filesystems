﻿#pragma once

template <class T>
class Iterator
{
public:
	explicit Iterator(const long collectionSize)
	{
		Size = collectionSize;
		CurrentPosition = 1;
	};

	virtual T* GetNext() = 0;
	virtual bool HasMore() = 0;

	virtual long GetCurrentPosition() const
	{
		return CurrentPosition;
	}
protected:
	long Size;
	long CurrentPosition;
};
