#pragma once
#include "Iterator.h"
#include "Cluster.h"
#include "ClusterIterator.h"

class EvenClusterIteratorDecorator: public Iterator<Cluster>
{
public:
	EvenClusterIteratorDecorator(const long collectionSize, ClusterIterator* iterator) : Iterator<Cluster>(collectionSize)
	{
		_iterator = iterator;
	}

	Cluster* GetNext() override;
	bool HasMore() override;
	long GetCurrentPosition() const override
	{
		return _iterator->GetCurrentPosition();
	};
private:
	ClusterIterator* _iterator;
};

