#pragma once
#include <windows.h>;
#include <cstdio>
#include <iostream>

class Cluster final
{
public:
	Cluster(const long clusterSize, BYTE *buffer)
	{
		_clusterSize = clusterSize;
		_data = buffer;
	}

	void Show() const
	{
		for (auto i = 1; i < _clusterSize + 1; i++) {
			printf("%02x ", _data[i - 1]);

			if (i % 16 == 0) {
				std::cout << std::endl;
			}
			else if (i % 8 == 0)
			{
				std::cout << "  ";
			}
		}
		std::cout << std::endl;
	}

private:
	long _clusterSize;
	BYTE* _data;
};

