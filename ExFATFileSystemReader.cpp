#include "pch.h"
#include "ExFATFileSystemReader.h"

using namespace std;
void ExFATFileSystemReader::ShowFileSystemInfo()
{
	cout << "Bytes per sector: " << this->_fileSystemInfo->BytesPerSector << endl;
	cout << "Sectors per cluster: " << static_cast<int>(this->_fileSystemInfo->SectorsPerCluster) << endl;
	cout << "Bytes per cluster: " << this->_fileSystemInfo->BytesPerCluster << endl;
	cout << "Total sectors: " << this->_fileSystemInfo->TotalSectors << endl;
	cout << "Total clusters: " << this->_fileSystemInfo->TotalClusters << endl;
}
