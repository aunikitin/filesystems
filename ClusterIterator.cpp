#include "pch.h"
#include "ClusterIterator.h"
#include <windows.h>;

Cluster* ClusterIterator::GetNext()
{
	SetPreviousCluster();
	const DWORD clusterPosition = (CurrentPosition - 1)*_clusterSize;
	BYTE *buffer = ReadData(clusterPosition, _clusterSize);
	_current = new Cluster(_clusterSize, buffer);
	CurrentPosition++;

	return _current;
}

bool ClusterIterator::HasMore()
{
	return CurrentPosition < Size;
}

BYTE* ClusterIterator::ReadData(const DWORD startPosition, const DWORD bytesToRead) const
{
	SetFilePointerEx(this->_handle, { startPosition }, nullptr, FILE_BEGIN);

	BYTE *buffer = new BYTE[bytesToRead];
	DWORD bytesRead;
	const bool readResult = ReadFile(
		this->_handle,
		buffer,
		bytesToRead,
		&bytesRead,
		nullptr
	);

	if (!readResult || bytesRead != bytesToRead)
	{
		throw std::runtime_error("Cann't read data.");
	}

	return buffer;
}

void ClusterIterator::SetPreviousCluster()
{
	if(_current != nullptr)
	{
		delete _previous;
		_previous = _current;
	}
}
