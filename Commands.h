﻿#pragma once

enum Commands
{
	WrongCommand = 0,
	Exit = 1,
	EnterClusterNumber = 2,
	ShowAll = 3,
	TryDecorator = 4,
};
