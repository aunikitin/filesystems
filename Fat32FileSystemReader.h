#pragma once
#pragma pack(push, 1)
#include "BaseFileSystemReader.h"

struct Fat32BaseSystemInfo
{
	BYTE Padding[3];
	BYTE OemName[8];
};

struct Fat32FileSystemInfo final : Fat32BaseSystemInfo
{
	WORD BytesPerSector;
	BYTE SectorsPerCluster;
	WORD ReservedSectors;
	BYTE NumbersOfFat;
	WORD RootEntityCount;
	WORD TotalSectors16;
	BYTE UnusedData2;
	WORD FatSize16;
	BYTE UnusedData3[8];
	DWORD TotalSectors32;
	DWORD FatSize32;

	LONGLONG TotalSectors;
	WORD BytesPerCluster;
	LONGLONG TotalClusters;
};

#pragma pack(pop)

class Fat32FileSystemReader final :
	public BaseFileSystemReader
{
public:
	explicit Fat32FileSystemReader(const HANDLE handle, BYTE *buffer) : BaseFileSystemReader(handle)
	{
		this->_fileSystemInfo = reinterpret_cast<Fat32FileSystemInfo*>(buffer);
		this->_fileSystemInfo->BytesPerCluster = this->_fileSystemInfo->BytesPerSector * this->_fileSystemInfo->SectorsPerCluster;
		this->_fileSystemInfo->TotalClusters = this->_fileSystemInfo->TotalSectors / this->_fileSystemInfo->SectorsPerCluster;
		TotalClusters = this->_fileSystemInfo->TotalClusters;
		Clusters = new ClusterCollection(handle, this->_fileSystemInfo->TotalClusters, this->_fileSystemInfo->BytesPerCluster);
	}
	~Fat32FileSystemReader() = default;
	void ShowFileSystemInfo() override;

	std::string GetFileSystemName() override
	{
		return "FAT32";
	}
private:
	Fat32FileSystemInfo* _fileSystemInfo;
	void FillMainFileSystemInfo() const;
};

