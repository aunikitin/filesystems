#include "pch.h"
#include "Cluster.h"
#include "ClusterIterator.h"
#include "BaseFileSystemReader.h"

BYTE* BaseFileSystemReader::ReadData(const DWORD startPosition, const DWORD bytesToRead) const
{
	SetFilePointerEx(this->Handle, { startPosition }, nullptr, FILE_BEGIN);

	BYTE *buffer = new BYTE[bytesToRead];
	DWORD bytesRead;
	const bool readResult = ReadFile(
		this->Handle,
		buffer,
		bytesToRead,
		&bytesRead,
		nullptr
	);

	if (!readResult || bytesRead != bytesToRead)
	{
		throw std::runtime_error("Cann't read data.");
	}

	return buffer;
}
void BaseFileSystemReader::ShowData(int clusterNumber) const
{
	if (clusterNumber < 1 || clusterNumber > TotalClusters)
	{
		std::cout << "Cluster number is incorrect. Try other value." << std::endl;
		return;
	}

	ClusterIterator* iterator = Clusters->GetIterator();
	iterator->SetClusterNumber(clusterNumber);
	Cluster* cluster = iterator->GetNext();
	cluster->Show();
}

void BaseFileSystemReader::ShowData() const
{
	ClusterIterator* iterator = Clusters->GetIterator();
	iterator->SetClusterNumber(1);
	while (iterator->HasMore())
	{
		Cluster* cluster = iterator->GetNext();
		cluster->Show();
	}
}
