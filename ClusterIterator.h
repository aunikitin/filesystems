#pragma once
#include "Iterator.h"
#include "Cluster.h"

class ClusterIterator: public Iterator<Cluster>
{
public:
	ClusterIterator(const HANDLE handle, const long collectionSize, const long clusterSize) : Iterator<Cluster>(collectionSize)
	{
		_handle = handle;
		_clusterSize = clusterSize;
		CurrentPosition = 1;
		_current = nullptr;
		_previous = nullptr;
	}

	~ClusterIterator()
	{
		delete _current;
		delete _previous;
	}

	void SetClusterNumber(const long newClusterNumber)
	{
		CurrentPosition = newClusterNumber;
	}

	Cluster* GetNext() override;
	bool HasMore() override;
private:
	Cluster* _current;
	Cluster* _previous;
	HANDLE _handle;

	long _clusterSize;

	BYTE* ReadData(const DWORD startPosition, const DWORD bytesToRead) const;
	void SetPreviousCluster();
};

